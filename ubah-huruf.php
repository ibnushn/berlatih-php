<?php
//kode di sini

function ubah_huruf($string){
    $alphabet = "abcdefghijklmnopqrstuvwxyz";
    // $length = strlen($alphabet);
    $tampung = "";
    for($i = 0; $i < strlen($string); $i++){
        $position = strpos($alphabet, $string[$i]);
        $tampung .=  $alphabet[$position+1];
    }
    echo $string . "= $tampung<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>